# REACT APP

### WHAT INSIDE THIS REPOS
This repo contains the frontend UI for **THE CHAT** app.

### How to use the project
Before starting this project you need to run the Java server running.
To start this project, you can simply use at the root of the project : 
```shell
npm start
```

### Repo Structure
Our UI is splited into three main pages. Each pages is composed of section, and each section is composed of components, this ensure high modularity and reutilisability our code.
In addition we use wustom css files when needed to apply specific style to our components.