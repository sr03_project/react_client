import React, {useEffect, useState} from "react";
import MessageList from "../components/MessageList";
import Footer from "../components/Footer";
import {useLocation, useNavigate} from "react-router-dom";
import '../css/Channel.css';
import MemberList from "../components/MemberList";
import Config from "../Config";


const Channel = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const userData = location.state.userData;
    const name = location.state.name;
    const channelId = location.state.channelId;
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const [ws, setWs] = useState(null);
    const [connectedMembers, setConnectedMembers] = useState([])

    // fonction used to establish the connection with the java server only one time using websocket.
    useEffect(() => {
        const websocket = new WebSocket(Config.webSocketAddress+"salons/"+channelId+"/"+userData.id);

        websocket.onopen = () => {
            console.log('WebSocket is connected');
        };

        websocket.onmessage = (evt) => {
            const message = JSON.parse(evt.data);
            if (message.hasOwnProperty('connectedMembers')) {
                if (message.hasOwnProperty('message')) {
                    setMessages((prevMessages) => [...prevMessages, message]); // update the message list
                }
                const cm = message.connectedMembers;
                setConnectedMembers(cm) // update the connected user every time we receive a message.
            } else {
                setMessages((prevMessages) => [...prevMessages, message]);
            }
        };


        websocket.onclose = () => {
            console.log('WebSocket is closed');
        };

        setWs(websocket)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const sendMessage = () => {
        if (ws) {
            const now = new Date();

            const hours = now.getHours();
            const minutes = now.getMinutes();
            const formattedTime = `${hours}:${minutes}`;
            const jsonData = JSON.stringify({
                user: userData.firstName,
                message: message,
                timestamp: formattedTime,
            });
            ws.send(jsonData);
            document.getElementById("sendBtn").value = ''
        }
    };

    const handleKeyDown = (e) => { // allow user to send the message when he press enter inside the input field.
        if (e.key === 'Enter') {
            sendMessage();
        }
    };

    return (
        <div className="channel">
            <header className="m-5">
                <div className="self-center">
                    <button className="btn btn-active btn-neutral" onClick={ (e)=> {
                        ws.close();
                        navigate("/Home", {state: {userData: userData}},);
                    }
                    }>Disconnect</button>
                    <h1 className="self-center text-center text-4xl font-extrabold underline underline-offset-3 decoration-8 decoration-blue-400">
                        {name}
                    </h1>
                </div>
            </header>
            <div className="flex flex-row h-screen">
            <MemberList channelId={channelId} connectedMembers={connectedMembers}/>
                <main className="flex flex-col">
                    <MessageList messages={messages} user={userData.firstName}/>
                    <div className="sender mt-auto">
                        <input
                            type="text"
                            id="sendBtn"
                            placeholder="Type here"
                            className="input input-bordered w-full max-w-xs"
                            onChange={(e) => setMessage(e.target.value)}
                            onKeyDown={handleKeyDown}
                        />
                        <button className="btn btn-active btn-neutral" onClick={sendMessage}>Send</button>
                    </div>
                </main>
            </div>
            <Footer/>
        </div>
    );
}

export default Channel;