import {useLocation} from "react-router-dom";
import Sidebar from "../components/Sidebar";
import Footer from "../components/Footer";
import Header from "../components/Header";
import "../css/Home.css"
import All from "../sections/All";
import MyChat from "../sections/MyChat";
import Invitations from "../sections/Invitations";
import Profile from "../sections/Profile";
import {useEffect} from "react";

// main page when connected
const Home = () => {
    const location = useLocation();
    const userData = location.state.userData; // Get the user data pass as state

    return (
        <div id="Homediv">
            <Header user={userData} />
            <div className="flex flex-row h-screen">
                <Sidebar user={userData} />
                <main className="mt-20">
                    <All user={userData}></All>
                    <MyChat user={userData}></MyChat>
                    <Invitations user={userData}></Invitations>
                    <Profile user={userData}></Profile>
                </main>
            </div>
            <Footer/>
        </div>
    )
}

export default Home;