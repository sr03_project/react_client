import ChatItem from "../components/ChatItem";
import React, {useEffect, useState} from 'react';
import axios from "axios";
import InvitationForm from "../components/InvitationForm";
import Config from "../Config";
import {useNavigate} from "react-router-dom";

/** 
 * This components render the list of chat of which user has admin rights.
 * Every chatItem has a Go, Edit, Remove and Invite button. 
 * @param {user} user the current user
  */
const MyChat = ({ user }) => {
    const navigate = useNavigate();
    const [channels, setChannels] = useState([])

    const loadChannel = async () => {
        const url = Config.serverAddress + "/api/channels/listChannelsOfCreator/" + user.id
        try {
            const response = await axios.get(url);
            if (response.data) {
                setChannels(response.data);
            } else {
                console.log("Load channel failed");
            }
        } catch (error) {
            console.error("Error loading channels:", error);
        }
    }

    const removeChannel = async (channelId) => {
        const url = Config.serverAddress + "/api/channels/removeChannel/" + channelId
        try {
            const response = await axios.post(url);
            if (response.data) {
                await loadChannel();
            } else {
                console.log("Remove channel failed");
            }
        } catch (error) {
            console.error("Error removing channel:", error);
        }
    }

    useEffect(() => {
        loadChannel().then(()=>{}) // load channels when the page is loaded.
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div id="Mychat">
            <div className="self-center m-30">
                <h1 className="self-center text-center text-4xl font-extrabold decoration-8 decoration-blue-400">
                    My Chat</h1>
            </div>
            <ul className="bg-white shadow overflow-hidden sm:rounded-md max-w-sm mx-auto mt-16">
                {channels.map((chat, index) => (
                    <ChatItem
                        key={index}
                        title={chat.title}
                        description={chat.description}
                        actions={
                            <div className="mt-4 flex items-center justify-end">
                                <button className="btn btn-xs btn-success m-1" onClick={(e) => {
                                    navigate("/Channel", {state: {userData: user, name: chat.title, channelId: chat.id}},);
                                }
                                }>Go
                                </button>
                                <button className="btn btn-xs btn-info m-1" onClick={
                                    (e) => {
                                        navigate("/channelForm", {state: {user: user, channel: chat}});
                                    }
                                }>Edit
                                </button>
                                <InvitationForm channelId={chat.id} userId={user.id}/>
                                <button className="btn btn-xs btn-error m-1" onClick={
                                    (e) => {
                                        removeChannel(chat.id).then();
                                        window.location.reload();
                                    }
                                }>Delete
                                </button>
                            </div>
                        }
                    />
                ))}
            </ul>
        </div>)
}

export default MyChat;
