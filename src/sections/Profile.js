import React, { useEffect, useState } from 'react';
import axios from 'axios';
import StatusCode from 'http-status-codes'
import {useNavigate} from "react-router-dom";
import Config from "../Config";

/** 
 * This component render user information and allow user to update these information.
 * @param {user} user the current user.
 */
const Profile = ({ user }) => {
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: '', // user should enter his password inside each request in order to prevent allow this operation
        admin: user.admin,
        newPassword: '', // to fill only if we want to update password to.
    });

    const updateProfile = async (e) => {
        e.preventDefault();
        if (!validateForm()) {
            alert("form not valid");
            return;
        }
        try {
            const response = await axios.postForm( Config.serverAddress + '/api/users/updateUser', formData);
            alert("Update done !")
            navigate("/home", {state: {userData: response.data}})
        } catch (error) {
            if (error.response.status === StatusCode.BAD_REQUEST) {
                alert("User not found");
            } else if (error.response.status === StatusCode.UNAUTHORIZED) {
                alert("Wrong password");
            } else {
                alert("Cannot update data")
            }
        }
    };

    // Update the value of formData whenever a field is filled
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    // form Validation functions
    const showError = (input, messages) => {
        const errorSpan = input.parentElement.querySelector(".error-message");
        if (Array.isArray(messages)) {
            const ul = document.createElement("ul");
            messages.forEach(msg => {
                const li = document.createElement("li");
                li.textContent = msg;
                ul.appendChild(li);
            });
            errorSpan.innerHTML = ""; // Clear previous messages
            errorSpan.appendChild(ul);
        } else {
            errorSpan.textContent = messages;
        }
        input.classList.add("border-red-600");
    };

    const clearError = (input) => {
        const errorSpan = input.parentElement.querySelector(".error-message");
        errorSpan.textContent = "";
        input.classList.remove("border-red-600");
    };

    const validateEmail = (email) => {
        const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return re.test(String(email).toLowerCase());
    };

    const getPasswordErrorMessage = (password) => {
        let messages = [];
        if (password === "") {
            return "null value will preserve the last password";
        }
        if (!/.{8,}/.test(password)) {
            messages.push("Password must be at least 8 characters long.");
        }
        if (!/[a-z]/.test(password)) {
            messages.push("Password must contain at least one lowercase letter.");
        }
        if (!/[A-Z]/.test(password)) {
            messages.push("Password must contain at least one uppercase letter.");
        }
        if (!/\d/.test(password)) {
            messages.push("Password must contain at least one digit.");
        }
        if (!/[@$!%*?&]/.test(password)) {
            messages.push("Password must contain at least one special character (@$!%*?&).");
        }
        return messages;
    };

    const validateInput = (input) => {
        if (input.name === "email" && !validateEmail(input.value)) {
            showError(input, "Please enter a valid email address.");
            return false;
        } else if (input.name === "password" || input.name === "newPassword") {
            const passwordErrors = getPasswordErrorMessage(input.value);
            if (passwordErrors.length > 0 && input.value !== "") {
                showError(input, passwordErrors);
                return false;
            }
        } else if (input.value.trim() === "") {
            showError(input, "This field is required.");
            return false;
        }
        clearError(input);
        return true;
    };

    const validateForm = () => {
        const form = document.getElementById("userForm");
        const inputs = form.querySelectorAll("input[required], input[type='email'], input[type='password']");
        let isValid = true;
        inputs.forEach(input => {
            if (!validateInput(input)) {
                isValid = false;
            }
        });
        return isValid;
    };

    useEffect(() => {
        const form = document.getElementById("userForm");
        const inputs = form.querySelectorAll("input[required], input[type='email'], input[type='password']");
        inputs.forEach(input => {
            input.addEventListener("input", () => validateInput(input));
        });
    });

    return (
        <div id="Profile" className="h-100%">
            <div className="self-center m-30">
                <h1 className="self-center text-center text-4xl font-extrabold underline underline-offset-3 decoration-8 decoration-blue-400">{user.firstName}'s
                    profile</h1>
            </div>
            <form id="userForm" className="flex-1 max-w-md mx-auto mt-10" onSubmit={updateProfile}>
                <div className="grid md:grid-cols-2 md:gap-6">
                    <div className="relative z-0 w-full mb-5 group">
                        <input type="hidden" value={user.id} name="id"/>
                        <input type="text" name="firstName" id="floating_first_name"
                               onChange={handleChange}
                               value={formData.firstName}
                               className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                               placeholder=" " required/>
                        <label htmlFor="floating_first_name"
                               className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">First
                            name</label>
                        <span className="error-message text-red-600"></span>
                    </div>
                    <div className="relative z-0 w-full mb-5 group">
                        <input type="text" name="lastName" id="floating_last_name"
                               onChange={handleChange}
                               value={formData.lastName}
                               className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                               placeholder=" " required/>
                        <label htmlFor="floating_last_name"
                               className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Last
                            name</label>
                        <span className="error-message text-red-600"></span>
                    </div>
                </div>
                <div className="relative z-0 w-full mb-5 group">
                    <input type="email" name="email" id="email"
                           onChange={handleChange}
                           value={formData.email}
                           className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                           placeholder=" " required/>
                    <label htmlFor="email"
                           className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Email
                        address</label>
                    <span className="error-message text-red-600"></span>
                </div>
                <div className="relative z-0 w-full mb-5 group">
                    <input type="password" name="password"
                           onChange={handleChange}
                           value={formData.password}
                           id="password"
                           className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                           placeholder=" "/>
                    <label htmlFor="password"
                           className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                        Old Password</label>
                    <span className="error-message text-red-600"></span>
                </div>
                <div className="relative z-0 w-full mb-5 group">
                    <input type="password" name="newPassword"
                           onChange={handleChange}
                           value={formData.newPassword}
                           id="newPassword"
                           className="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                           placeholder=" "/>
                    <label htmlFor="newPassword"
                           className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                        New Password</label>
                    <span className="error-message text-red-600"></span>
                </div>
                <button type="submit"
                        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Update
                </button>
            </form>
            <div className="flex flex-col items-center justify-between">
                <h3 className="text-lg leading-6 font-medium text-gray-900">Hint</h3>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">Always enter your password in old Password field for updating data</p>
                <p className="mt-1 max-w-2xl text-sm text-gray-500">If you don't want to change your password, leave the New Password field empty</p>
            </div>
        </div>
    );
};

export default Profile;
