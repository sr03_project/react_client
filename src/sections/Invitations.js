import React, {useEffect, useState} from 'react';
import ChatItem from "../components/ChatItem";
import axios from "axios";
import Config from "../Config";

const Invitations = ({ user }) => {
    const [invitations, setInvitations] = useState([]);

    const acceptInvitation = async (invitation) => {
        const url = Config.serverAddress + "/api/invitations/accept/" + user.id ;
        const response = await axios.patch(url, invitation)
        if (response.status !== 200) {
            console.log("Cannot accept invitations !")
            alert("Cannot accept invitation, retry later")
        }
        alert("Invitation accepted!")
        window.location.reload() // reload page to load changes.
    }

    const refuseInvitation= async (invitation) => {
        const url = Config.serverAddress + "/api/invitations/refuse/" + user.id ;
        const response =   await axios.patch(url, invitation)
        if (response.status!== 200) {
            console.log("Cannot refuse this invitation !!!")
            alert("Cannot refuse this invitation, retry later !")
        }
        alert("Invitation refused")
        window.location.reload() // relaod the page.
    }

    useEffect(() => { // load invitations at the loading of the page.
        const loadInvitations = async () => {
            const url = Config.serverAddress + "/api/invitations/listForUser/" + user.id
            const response = await axios.get(url)
            if (!response.data) {
                console.log("Load channel failed")
                return
            }
            setInvitations(response.data)
        }

        loadInvitations().then(() => {})
    },[user.id]);

    return (
        <div id="Invitations">
            <div className="self-center m-30">
                <h1 className="self-center text-center text-4xl font-extrabold underline underline-offset-3 decoration-8 decoration-blue-400">My
                    Invitations</h1>
            </div>
            <ul className="bg-white shadow overflow-hidden sm:rounded-md max-w-sm mx-auto mt-16">
                {invitations.map((invitation, index) => (
                    <ChatItem
                        key={index}
                        title={invitation.channel.title}
                        inviter={invitation.inviter.firstName}
                        description={invitation.channel.description}
                        actions={
                            <div className="mt-4 flex items-center justify-end">
                                <button className="btn btn-xs btn-success m-1" onClick={
                                    // TODO: accept an invitation
                                    (e) => {
                                        acceptInvitation(invitation).then( () => {} )
                                    }
                                }>Accept
                                </button>
                                <button className="btn btn-xs btn-error"
                                        onClick={() => {
                                            refuseInvitation(invitation).then(() => {
                                            })
                                        }}
                                >Refuse
                                </button>
                            </div>
                        }
                    />
                ))}
            </ul>
        </div>
    )
}


export default Invitations;
