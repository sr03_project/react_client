import ChatItem from "../components/ChatItem";
import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import axios from "axios";
import InvitationForm from "../components/InvitationForm";
import Config from "../Config";


const All = ({ user }) => {
    const navigate = useNavigate();
    const [channels, setChannels] = useState([])

    const loadChannel = async () => {
        const url = Config.serverAddress + "/api/channels/listChannels/" + user.id
        try {
            const response = await axios.get(url);
            if (response.data) {
                setChannels(response.data);
            } else {
                console.log("Load channel failed");
            }
        } catch (error) {
            console.error("Error loading channels:", error);
        }
    }

    const leaveChannel = async (userId, channelId) => {
        const url = Config.serverAddress + "/api/channels/leaveChannel/" + userId+"/"+channelId
        try {
            const response = await axios.post(url);
            console.log(response)
            if (response.data) {
                await loadChannel();
            } else {
                console.log("Remove channel failed");
            }
        } catch (error) {
            console.error("Error removing channel:", error);
        }
    }

    useEffect(() => {
        loadChannel().then(()=>{}) // ensure that this function is called at least one time when the page is loaded.
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
    <div id="All">
        <div className="self-center m-30">
            <h1 className="self-center text-center text-4xl font-extrabold decoration-8 decoration-blue-400">All</h1>
        </div>
        <ul className="bg-white shadow overflow-hidden sm:rounded-md max-w-sm mx-auto mt-16">
            {channels.map((chat, index) => (
                <ChatItem
                    key={index}
                    title={chat.title}
                    description={chat.description}
                    actions={
                        <div className="mt-4 flex items-center justify-end">
                            <button className="btn btn-xs btn-success m-1" onClick={(e) => {
                                navigate("/Channel", {state: {userData: user, name: chat.title, channelId: chat.id}},);
                            }
                            }
                            >Go
                            </button>
                            <InvitationForm channelId={chat.id} userId={user.id}/>
                            <button className="btn btn-xs btn-error m-1" onClick={
                                (e) => {
                                    leaveChannel(user.id, chat.id).then();
                                    window.location.reload();
                                }
                            }>Leave
                            </button>
                        </div>
                    }
                />
            ))}
        </ul>
    </div>)
}

export default All;
