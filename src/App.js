import React, {useEffect, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import './css/Channel.css'
import axios from "axios";
import Config from "./Config";

const StatusCode = require('http-status-codes');

/**
 * This component renders a login page and allow user to authenticate theirself.
 * 
 */
const App = () => {
    const navigate = useNavigate();
    const [errorMsg, setErrorMsg] = useState('');
    const [formData, setFormData] = useState({
        email: '',
        password: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            // use the java authenticate endpoint to verify if the user exists or not.
            const response = await axios.post( Config.serverAddress + '/api/users/authenticate', formData);
            if ( response.data ) {
                // Redirect to dedicated page with user information
                navigate('/home', { state: { userData: response.data } }); // send user data as state
            }
        } catch (error) {
            const responseStatus = error.response.status;
            if (responseStatus === StatusCode.BAD_REQUEST ) {
                setErrorMsg("User not found.");
                console.log("User not found");
            } else if (responseStatus === StatusCode.UNAUTHORIZED) {
                setErrorMsg("Wrong password");
                console.log("Wrong password");
            } else {
                setErrorMsg("Unknown error");
                console.log("Unknown error");
            }
        }
    };

    return (
        <section className="bg-gray-50 dark:bg-gray-900 self-center">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <div className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                    The chat
                </div>
                <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl text-center font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            Log in
                        </h1>
                        <span className="font-medium text-sm text-red-800">
                            {errorMsg}
                        </span>
                        <form className="space-y-4 md:space-y-6" method="post" onSubmit={handleSubmit} onChange={handleChange}>
                            <div>
                                <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
                                <input type="email" name="email" id="email" value={formData.email} onChange={handleChange} className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@company.com" required />
                            </div>
                            <div>
                                <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Password</label>
                                <input type="password" name="password" id="password" value={formData.password} onChange={handleChange} placeholder="••••••••" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
                            </div>
                            {/* Not already implemented
                            <div className="flex items-center justify-between">
                                <div className="flex items-start">
                                    <div className="flex items-center h-5">
                                        <input id="remember" aria-describedby="remember" type="checkbox" className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800" />
                                    </div>
                                    <div className="ml-3 text-sm">
                                        <label htmlFor="remember" className="text-gray-500 dark:text-gray-300">Remember me</label>
                                    </div>
                                </div>
                                <a href="google.com" className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500">Forgot password?</a>
                            </div> */}
                            <button type="submit" className="btn self-center btn-success">Log in</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default App;
