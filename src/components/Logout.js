import {useLocation} from "react-router-dom";

const Logout = () => {
    const location = useLocation()

    const logout = () => {
        location.state.user = undefined // invalidate the user.
        localStorage.removeItem('user');
        window.location.replace("http://localhost:3000/")
    }

    return (
        <div id="Logout">
            <button type="submit" onClick={logout} className="btn btn-outline btn-error">Logout</button>
        </div>
    )
}

export default Logout;