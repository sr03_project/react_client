import React, {useEffect} from "react";
import axios from "axios";
import Config from "../Config";
import '../css/MemberList.css';


// Aside components for Channels page, it allows to list every user of a channel.
const MemberList = ({channelId, connectedMembers}) => {
    const [members, setMembers] = React.useState([]);

    useEffect(() => {
        const getMembers = async () => {
            const url = Config.serverAddress + "/api/channels/listMembers/" + channelId ;
            const response = await axios.get(url);
            if (response.status !== 200) {
                console.log("Cannot get members");
                return
            }
            setMembers(response.data);
        }
        getMembers().then(() => {})
    }, [channelId]);

    function isIn(firstname, lastname, members) {
        return members.some(member =>
            member.firstName === firstname && member.lastName === lastname
        );
    }

    return (
        <aside className="z-0 w-1/4" aria-label="Sidebar">
            <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
                <h2 className="text-xl text-center m-5 font-extrabold">Members</h2>
                <ol className="ps-5 mt-2 space-y-1 list-decimal list-inside">
                    {
                        members.map((member, index) => (
                            <li key={index}>
                                {member.firstName} {member.lastName}
                                <span className={`status-indicator ${isIn(member.firstName, member.lastName, connectedMembers) ? 'connected' : 'disconnected'}`}></span>
                            </li>
                        ))
                    }
                </ol>
            </div>
        </aside>
    )
}

export default MemberList;