import React from 'react';
import Logout from "./Logout";

/** 
 * Simple sidebar with a logout button option
 * @param {user} userData the current user 
 * */ 
const Sidebar = (userData) => {
    const user = userData.user

    return (
        <aside className="z-0 w-1/4 mt-10" aria-label="Sidebar">
            <div className="h-full px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
                <ul className="space-y-2 font-medium items-center text-center">
                    <li>
                        <h2 className="text-xl text-center m-5 font-extrabold">User</h2>
                    </li>
                    <li>{user.firstName}</li>
                    <li>{user.lastName}</li>
                    <li>{user.email}</li>
                    <li className="align-baseline">
                    </li>
                    <Logout/>
                </ul>
            </div>
        </aside>
    );
}

export default Sidebar;
