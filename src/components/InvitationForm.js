import React, { useState } from 'react';
import Modal from 'react-modal';
import '../css/InvitationForm.css'
import axios from "axios";
import Config from "../Config";

const httpStatus= require('http-status-codes');

Modal.setAppElement('#root');

// InvitationForm is a popup that allow user to send invitation to a specified email.
const InvitationForm = ({channelId, userId}) => {
    const [popupOpen, setPopupOpen] = useState(false);
    const [email, setEmail] = useState('');

    const openPopup = () => {
        setPopupOpen(true);
    };

    const closePopup = () => {
        setPopupOpen(false);
    };

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const validateEmail = (email) => {
        const re = /\S+@\S+\.\S+/;
        return re.test(email);
    };

    const handleSubmit = async () => {
        if (validateEmail(email)) {
            document.getElementById("spanError").textContent = "";
            const formData = {
                inviterID: userId,
                invitedEmail: email,
                channelID: channelId,
            };
            closePopup();
            setEmail("")

            try {
                const url = Config.serverAddress + "/api/invitations/invite/";
                const response = await axios.postForm(url, formData);

                if (response.status === 200) {
                    alert("Invitation successfully sent.");
                }
            } catch (error) {
                console.log(error)
                if (error.response.status === httpStatus.BAD_REQUEST) {
                    alert("Email not found in our database");
                } else if (error.response.status === httpStatus.UNAUTHORIZED) {
                    alert("You are not members of this channel or the requested user is already in the channel");
                } else if (error.response.status === httpStatus.CONFLICT) { // CONFLICT
                    alert("User already has an invitation to this channel");
                } else {
                    alert("Invitation failed.");
                }
            }
        } else {
            document.getElementById("spanError").textContent = "Please enter a valid email address";
        }
    };

    return (
        <div>
            <button onClick={openPopup} className="btn btn-xs btn-info">Invite</button>
            <Modal
                isOpen={popupOpen}
                onRequestClose={closePopup}
                contentLabel="Request Email"
                className="react-modal-content"
                overlayClassName="react-modal-overlay"
            >
                    <div className="relative z-0 w-full mb-5 group">
                        <label htmlFor="floating_email"
                               className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:start-0 rtl:peer-focus:translate-x-1/4 rtl:peer-focus:left-auto peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                            Inviter Email address
                        </label>
                        <input
                            type="email"
                            name="email" id="floating_email"
                            value={email}
                            placeholder=" "
                            onChange={handleEmailChange}
                            className="mb-2 block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                        required/>
                        <span id="spanError" className="error-message text-red-600"></span>
                    </div>
                        <button onClick={handleSubmit} className="btn btn-sm btn-success mr-2"
                        >Validate
                        </button>
                        <button onClick={closePopup} className="btn btn-sm btn-error">Close</button>
            </Modal>
        </div>
);
};

export default InvitationForm;
