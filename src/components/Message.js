import React from 'react';

/**
 * render a message item inside a message box, this component automatically align left or right depending on 
 * the message.user and the user 
 * @param {message} message the message to display
 * @param {user} user the current user
 *    */

const Message = ({ message, user }) => {
    return (
        <div className={`chat ${message.user === user ? 'chat-end' : 'chat-start'}`}>
            <div className="chat-header">
                {message.user}
                <time className="text-xs opacity-50"> | {message.timestamp} </time>
            </div>
            <div className="chat-bubble">{message.message}</div>
        </div>
    );
};

export default Message;
