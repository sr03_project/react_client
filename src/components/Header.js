import React, { useState } from 'react';
import {useNavigate} from "react-router-dom";

const Header = (userData) => {
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const navigate = useNavigate();

    const handleToggleDropdown = () => {
        setDropdownOpen(!dropdownOpen);
    };
    // Allow the nav bar to toggle section when click on a link
    const handleSection = (e) => {
        e.preventDefault();
        const targetId = e.target.getAttribute('href').substring(1);
        const targetDiv = document.getElementById(targetId);
        const allDivs = document.querySelectorAll('main > div')

        allDivs.forEach(div => {
            div.style.display = 'none';
        })
        targetDiv.style.display = 'block';
    }

    return (
        <header>
            <div className="fixed z-40 top-0 navbar bg-base-100">
                <div className="navbar-start">
                    <div className="dropdown">
                        <div tabIndex="0" role="button" className="btn btn-ghost lg:hidden"
                             onClick={handleToggleDropdown}>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none"
                                 viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                      d="M4 6h16M4 12h8m-8 6h16"/>
                            </svg>
                        </div>
                        <ul tabIndex="0"
                            className={`menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52 ${dropdownOpen ? 'block' : 'hidden'}`}>
                            <li><a href="#All" className="active nav-items" onClick={handleSection}>All</a></li>
                            <li><a href="#Mychat" className="nav-items" onClick={handleSection}>My Chat</a></li>
                            <li><a href="#Invitations" className="nav-items" onClick={handleSection}>Invitations</a>
                            </li>
                            <li><a href="#Profile" className="nav-items" onClick={handleSection}>Profile</a>
                            </li>
                        </ul>
                    </div>
                    <div className="btn btn-ghost text-xl">CHAT SR03</div>
                </div>
                <div className="navbar-center hidden lg:flex">
                    <ul className="menu menu-horizontal px-1">
                        <li><a href="#All" className="nav-items" onClick={handleSection}>All</a></li>
                        <li><a href="#Mychat" className="nav-items" onClick={handleSection}>My Chat</a></li>
                        <li><a href="#Invitations" className="nav-items" onClick={handleSection}>Invitations</a></li>
                        <li><a href="#Profile" className="nav-items" onClick={handleSection}>Profile</a></li>
                    </ul>
                </div>
                <div className="navbar-end">
                    <button className="btn btn-sm" onClick={(e) => {
                        e.preventDefault()
                        navigate('/ChannelForm', {state: {user: userData.user }})
                    }}>
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30"
                             viewBox="0 0 50 50">
                            <title>New Channel</title>
                            <path
                                d="M 25 2 C 12.309295 2 2 12.309295 2 25 C 2 37.690705 12.309295 48 25 48 C 37.690705 48 48 37.690705 48 25 C 48 12.309295 37.690705 2 25 2 z M 25 4 C 36.609824 4 46 13.390176 46 25 C 46 36.609824 36.609824 46 25 46 C 13.390176 46 4 36.609824 4 25 C 4 13.390176 13.390176 4 25 4 z M 24 13 L 24 24 L 13 24 L 13 26 L 24 26 L 24 37 L 26 37 L 26 26 L 37 26 L 37 24 L 26 24 L 26 13 L 24 13 z"></path>
                        </svg>
                    </button>
                </div>
            </div>
        </header>
    );
}

export default Header;
