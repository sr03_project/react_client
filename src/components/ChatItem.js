import React from 'react';

/**
 * This component renders a chat as an item for a list <ol> or <ul>.
 *
 * @param {string} title The chat title.
 * @param {string} description The chat description.
 * @param {user} inviter Optional params for invitation list, The user who invite the current user to the chat.
 * @param {actions} actions list of buttons that should be displayed inside the item.
 * @returns {Function} A React element that renders a greeting to the user.
 */
const ChatItem = ({ title, description, inviter, actions }) => {
    return (
        <li className="border-t border-gray-200">
            <div className="px-4 py-5 sm:px-6 border-solid">
                <div className="flex items-center justify-between">
                    <h3 className="text-lg leading-6 font-medium text-gray-900">{title}</h3>
                    <p className="mt-1 max-w-2xl text-sm text-gray-500">{description}</p>
                </div>
                <div>
                    {inviter !== undefined && (
                        <p className="mt-1 max-w-2xl text-sm text-gray-500">From : {inviter}</p>
                    )}
                    {actions}
                </div>
            </div>
        </li>
    );
}

export default ChatItem;
