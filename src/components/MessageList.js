import React, { useEffect, useRef } from 'react';
import Message from './Message';

/**
 * return a list message box components that automatically scroll when a messages is passed 
 * @param {list<message>} messages the list of messages shown 
 * @param {user} user the current user 
 * */
const MessageList = ({ messages, user }) => {
    const messageBoxRef = useRef(null);

    useEffect(() => {
        // Scroll to the bottom of the message box whenever messages change
        messageBoxRef.current?.lastElementChild?.scrollIntoView({ behavior: "smooth" });
    }, [messages]);

    return (
        <div className="MessageBox" ref={messageBoxRef}>
            {messages.map((message, index) => (
                <Message key={index} message={message} user={user} />
            ))}
        </div>
    );
};

export default MessageList;
